const apiAuth = {
	appToken: null,
	userEmail: null,
	userName: null
};

async function getAPIAuth() {
	if (apiAuth.appToken == null) {
		let data = await fetch('/static/geo-api.json').then(out => out.json());

		apiAuth.appToken = data.appToken;
		apiAuth.userEmail = data.userEmail;
		apiAuth.userName = data.appToken;
	}

	return apiAuth;
}

async function getAPIAccessToken() {
	let accessData = await getAPIAuth();

	let accessToken = await fetch('https://www.universal-tutorial.com/api/getaccesstoken', {
		headers: new Headers({
			"Accept": "application/json",
			"Api-Token": accessData.appToken,
			"User-Email": accessData.userEmail
		})
	}).then(out => out.json()).then(out => out.auth_token);

	return `${accessData.userName} ${accessToken}`;
}

export async function getCountries() {
	return await fetch('https://www.universal-tutorial.com/api/countries/', {
		headers: new Headers({
			"Accept": "application/json",
			"Authorization": await getAPIAccessToken()
		})
	}).then(out => out.json())
}

export async function getStates(country) {
	return await fetch(`https://www.universal-tutorial.com/api/states/${country}`, {
		headers: new Headers({
			"Accept": "application/json",
			"Authorization": await getAPIAccessToken()
		})
	}).then(out => out.json())
}

export async function getCities(state) {
	return await fetch(`https://www.universal-tutorial.com/api/cities/${state}`, {
		headers: new Headers({
			"Accept": "application/json",
			"Authorization": await getAPIAccessToken()
		})
	}).then(out => out.json())
}