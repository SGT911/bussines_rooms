export function bAlert(type, data) {
	const alert = document.createElement('div');
	alert.classList.add('alert', `alert-${type}`, 'alert-dismissible', 'fade', 'show');

	alert.setAttribute('role', 'alert');

	const innerData = document.createElement('span');
	innerData.innerHTML = data;
	alert.appendChild(innerData);

	const closeButton = document.createElement('button')
	closeButton.classList.add('btn-close');
	closeButton.setAttribute('type', 'button');
	closeButton.setAttribute('data-bs-dismiss', 'alert');
	closeButton.setAttribute('aria-label', 'Close');
	alert.appendChild(closeButton);

	return alert
}

export function disableForm(form) {
	form.addEventListener('submit', evt => evt.preventDefault());
}

export function onLoad(fx) {
	window.addEventListener('load', fx);
}

export const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/