---------------------
-- (001) Structure --
---------------------

CREATE TABLE users (
	id SERIAL NOT NULL,
	identification BIGINT NOT NULL,
	passwd CHAR(129) NOT NULL,
	full_name VARCHAR(250) NOT NULL,
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	is_admin BOOL NOT NULL DEFAULT FALSE,

	PRIMARY KEY (id),
	UNIQUE (identification)
);

CREATE TABLE headquarters (
	id SERIAL NOT NULL,
	name VARCHAR(150) NOT NULL,
	country CHAR(2) NOT NULL,
	state VARCHAR(35) NOT NULL,
	city VARCHAR(30) NOT NULL,
	location_detail TEXT NOT NULL,

	PRIMARY KEY (id),
	UNIQUE (name)
);

CREATE TABLE rooms (
	hq_id INT NOT NULL,
	id INT NOT NULL,
	floor SMALLINT NOT NULL,
	name VARCHAR(35) NOT NULL,
	max_capacity INT NOT NULL,

	PRIMARY KEY(hq_id, id),
	UNIQUE (id, name),
	FOREIGN KEY (hq_id) REFERENCES headquarters (id)
);

CREATE TYPE age_range AS ENUM ('-18', '18-35', '35-60', '60+');
CREATE TABLE clients (
	id SERIAL NOT NULL,
	first_name VARCHAR(30) NOT NULL,
	second_name VARCHAR(30) NULL,
	first_last_name VARCHAR(30) NOT NULL,
	second_last_name VARCHAR(30) NULL,
	identification BIGINT NOT NULL,
	identification_vd SMALLINT NULL,
	country CHAR(2) NOT NULL,
	state VARCHAR(35) NOT NULL,
	city VARCHAR(30) NOT NULL,
	age age_range NOT NULL,
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),

	PRIMARY KEY (id),
	UNIQUE (identification, identification_vd)
);

CREATE TABLE cli_mails (
	id SERIAL NOT NULL,
	cli_id INT NOT NULL,
	username VARCHAR(60) NOT NULL,
	domain VARCHAR(40) NOT NULL,

	PRIMARY KEY (id),
	UNIQUE (cli_id, username, domain),
	FOREIGN KEY (cli_id) REFERENCES clients (id)
);

CREATE TABLE cli_phones (
	id SERIAL NOT NULL,
	cli_id INT NOT NULL,
	reg_code SMALLINT NOT NULL,
	number BIGINT NOT NULL,

	PRIMARY KEY (id),
	UNIQUE (cli_id, reg_code, number),
	FOREIGN KEY (cli_id) REFERENCES clients (id)
);


CREATE TABLE event_types (
	id SERIAL NOT NULL,
	name VARCHAR(100) NOT NULL,

	PRIMARY KEY (id),
	UNIQUE (name)
);

CREATE TABLE events (
	id SERIAL NOT NULL,
	usr_id INT NOT NULL,
	cli_id INT NOT NULL,
	hq_id INT NOT NULL,
	room_id INT NOT NULL,
	type_id INT NOT NULL,
	event_at TIMESTAMPTZ NOT NULL,
	people_count INT NOT NULL,
	is_confirmed BOOL NOT NULL DEFAULT FALSE,
	created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),

	PRIMARY KEY (id),
	FOREIGN KEY (usr_id) REFERENCES users (id),
	FOREIGN KEY (cli_id) REFERENCES clients (id),
	FOREIGN KEY (hq_id, room_id) REFERENCES rooms (hq_id, id),
	FOREIGN KEY (type_id) REFERENCES event_types (id)
);