----------------------
-- (003) Procedures --
----------------------


CREATE PROCEDURE create_user (_id BIGINT, _full_name VARCHAR(250), _passwd TEXT)
	LANGUAGE plpgsql
	AS $$
	BEGIN
		INSERT INTO users (identification, passwd, full_name)
			VALUES (_id, HASH_PASSWD(_passwd), LOWER(_full_name));
	END;
	$$;

CREATE PROCEDURE create_admin (_id BIGINT, _full_name VARCHAR(250), _passwd TEXT)
	LANGUAGE plpgsql
	AS $$
	DECLARE
		uid INT;
	BEGIN
		INSERT INTO users (identification, passwd, full_name)
			VALUES (_id, HASH_PASSWD(_passwd), LOWER(_full_name))
			RETURNING id INTO uid;

		UPDATE users SET is_admin = TRUE WHERE id = uid;
	END;
	$$;

CREATE PROCEDURE user_change_password (_id INT, _passwd TEXT)
	LANGUAGE plpgsql
	AS $$
	BEGIN
		UPDATE users SET passwd = HASH_PASSWD(_passwd) WHERE id = _id;
	END;
	$$;

CREATE PROCEDURE alter_user_admin(_id INT, _state BOOL)
	LANGUAGE plpgsql
	AS $$
	BEGIN
		UPDATE users
			SET is_admin = _state
			WHERE id = _id;
	END;
	$$;

CREATE PROCEDURE add_headquarter (
		_name VARCHAR(150),
		_country CHAR(2),
		_state VARCHAR(35),
		_city VARCHAR(30),
		_location_detail TEXT
	)
	LANGUAGE plpgsql
	AS $$
	BEGIN
		INSERT INTO headquarters (name, country, state, city, location_detail)
			VALUES (_name, UPPER(_country), _state, _city, _location_detail);
	END;
	$$;

CREATE PROCEDURE add_room (
		_headquarter INT,
		_floor SMALLINT,
		_name VARCHAR(35),
		_max_capacity INT
	)
	LANGUAGE plpgsql
	AS $$
	DECLARE
		room_id INT;
	BEGIN
		SELECT
			MAX(id) + 1 INTO room_id
		FROM rooms
			WHERE hq_id = _headquarter;

		IF room_id IS NULL THEN
			room_id = 1;
		END IF;

		INSERT INTO rooms (id, hq_id, floor, name, max_capacity)
			VALUES (room_id, _headquarter, _floor, _name, _max_capacity);
	END;
	$$;

CREATE PROCEDURE add_client_email (_cli_id INT, _email VARCHAR)
	LANGUAGE plpgsql
	AS $$
	DECLARE
		username VARCHAR = SPLIT_PART(_email, '@', 1);
		domain VARCHAR = SPLIT_PART(_email, '@', 2);
	BEGIN
		INSERT INTO cli_mails (cli_id, username, domain)
			VALUES (_cli_id, username, domain);
	END;
	$$;

CREATE PROCEDURE add_client_phone (_cli_id INT, _phone VARCHAR)
	LANGUAGE plpgsql
	AS $$
	DECLARE
		reg_code SMALLINT = SPLIT_PART(_phone, ' ', 1)::SMALLINT;
		number BIGINT = SPLIT_PART(_phone, ' ', 2)::BIGINT;
	BEGIN
		INSERT INTO cli_phones (cli_id, reg_code, number)
			VALUES (_cli_id, reg_code, number);
	END;
	$$;

CREATE PROCEDURE create_client (
		_first_name VARCHAR(30),
		_second_name VARCHAR(30),
		_first_last_name VARCHAR(30),
		_second_last_name VARCHAR(30),
		_identification BIGINT,
		_identification_vd SMALLINT,
		_country CHAR(2),
		_state VARCHAR(35),
		_city VARCHAR(30),
		_age age_range,
		_emails VARCHAR[],
		_phones VARCHAR[]
	)
	LANGUAGE plpgsql
	AS $$
	DECLARE
		cli_id INT;
		email VARCHAR;
		phone VARCHAR;
		second_name VARCHAR = NULL;
		second_last_name VARCHAR = NULL;
	BEGIN
		IF _second_name IS NOT NULL THEN
			second_name = LOWER(_second_name);
		END IF;

		IF _second_last_name IS NOT NULL THEN
			second_last_name = LOWER(_second_last_name);
		END IF;

		INSERT INTO clients (first_name, second_name, first_last_name, second_last_name, identification,
								identification_vd, country, state, city, age)
			VALUES (LOWER(_first_name), second_name, LOWER(_first_last_name), second_last_name, _identification,
					_identification_vd, UPPER(_country), _state, _city, _age)
			RETURNING id INTO cli_id;

		FOREACH email IN ARRAY _emails LOOP
			CALL add_client_email(cli_id, email);
		END LOOP;

		FOREACH phone IN ARRAY _phones LOOP
			CALL add_client_phone(cli_id, phone);
		END LOOP;
	END;
	$$;

CREATE PROCEDURE add_event_type (_name VARCHAR)
	LANGUAGE plpgsql
	AS $$
	BEGIN
		INSERT INTO event_types (name)
			VALUES (_name);
	END;
	$$;

CREATE PROCEDURE create_event (
		_usr_id INT,
		_cli_id INT,
		_hq_id INT,
		_room_id INT,
		_evt_type INT,
		_people_count INT,
		_event_at TIMESTAMPTZ
	)
	LANGUAGE plpgsql
	AS $$
	BEGIN
		INSERT INTO events (usr_id, cli_id, hq_id, room_id, type_id, event_at, people_count)
			VALUES (_usr_id, _cli_id, _hq_id, _room_id, _evt_type, _event_at, _people_count);
	END;
	$$;

CREATE PROCEDURE alter_confirm_event (_id INT, _confirm BOOL)
	LANGUAGE plpgsql
	AS $$
	BEGIN
		UPDATE events SET is_confirmed = _confirm WHERE id = _id;
	END;
	$$;