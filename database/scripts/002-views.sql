-----------------
-- (002) Views --
-----------------

CREATE VIEW auth_users
	AS SELECT
		id,
		identification,
		passwd,
		is_admin
	FROM users;

CREATE VIEW v_rooms
	AS SELECT
		rom.id,
		rom.hq_id,
		hq.name AS hq_name,
		hq.country,
		hq.state,
		hq.city,
		hq.location_detail,
		rom.floor,
		rom.name,
		rom.max_capacity
	FROM rooms AS rom
		INNER JOIN headquarters AS hq
			ON hq.id = rom.hq_id;

CREATE VIEW v_clients
	AS SELECT
		cli.id,
		CASE
			WHEN cli.identification_vd IS NULL
				THEN cli.identification::VARCHAR
			ELSE cli.identification || '-' || cli.identification_vd
		END AS identification,
		INITCAP(GET_FULLNAME(
			cli.first_name,
			cli.second_name,
			cli.first_last_name,
			cli.second_last_name
		)) AS full_name,
		cli.country,
		cli.state,
		cli.city,
		cli.age,
		cli.created_at,
		(SELECT
			ARRAY_AGG(ph.reg_code || ' ' || ph.number)
		FROM cli_phones AS ph
			WHERE cli_id = cli.id) AS phones,
		(SELECT
			ARRAY_AGG(ma.username || '@' || ma.domain)
		FROM cli_mails AS ma
			WHERE cli_id = cli.id) AS emails
	FROM clients AS cli
	ORDER BY cli.created_at ASC;

CREATE VIEW v_events
	AS SELECT
		evt.id,
		evt.cli_id,
		cli.identification AS cli_identification,
		cli.full_name AS cli_name,
		evt.usr_id,
		usr.identification AS usr_identification,
		usr.full_name AS usr_name,
		rom.hq_id,
		rom.id AS room_id,
		rom.hq_name,
		rom.name AS room_name,
		rom.floor AS room_floor,
		evtt.name AS event_type,
		evt.event_at,
		evt.people_count,
		evt.is_confirmed,
		evt.created_at
	FROM events AS evt
		INNER JOIN v_clients AS cli
			ON cli.id = evt.cli_id
		INNER JOIN users AS usr
			ON usr.id = evt.usr_id
		INNER JOIN v_rooms AS rom
			ON rom.id = evt.room_id AND rom.hq_id = evt.hq_id
		INNER JOIN event_types AS evtt
			ON evtt.id = evt.type_id
	ORDER BY evt.created_at ASC;

CREATE VIEW events_summary
	AS SELECT
		evt.id,
		evt.cli_name AS name,
		evt.cli_identification AS identification,
		cli.phones,
		rom.country,
		rom.state,
		rom.city,
		rom.location_detail,
		evt.event_at,
		evt.people_count,
		evt.event_type,
		evt.is_confirmed
		FROM v_events AS evt
			INNER JOIN v_clients AS cli
				ON cli.id = evt.cli_id
			INNER JOIN v_rooms AS rom
				ON rom.id = evt.room_id AND rom.hq_id = evt.hq_id
		ORDER BY evt.event_at DESC;

CREATE VIEW age_range_choices
	AS select UNNEST(ENUM_RANGE(ENUM_FIRST(NULL::age_range),NULL::age_range)::TEXT[]) AS choices;