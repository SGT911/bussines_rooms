---------------------------------
-- (000) Functions and Helpers --
---------------------------------

CREATE FUNCTION HASH_PASSWD(raw_txt TEXT)
	RETURNS VARCHAR
	AS $$
	BEGIN
		RETURN SUBSTR(SHA512(raw_txt::BYTEA)::VARCHAR, 3);
	END;
	$$ LANGUAGE plpgsql;

CREATE FUNCTION GET_FULLNAME(f_name VARCHAR, s_name VARCHAR, f_lname VARCHAR, s_lname VARCHAR)
	RETURNS TEXT
	AS $$
	DECLARE
		full_name TEXT = f_name;
	BEGIN
		IF s_name IS NOT NULL THEN
			full_name = full_name || ' ' || s_name;
		END IF;

		full_name = full_name || ' ' || f_lname;

		IF s_lname IS NOT NULL THEN
			full_name = full_name || ' ' || s_lname;
		END IF;

		RETURN full_name;
	END;
	$$ LANGUAGE plpgsql;