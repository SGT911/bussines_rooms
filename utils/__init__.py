from toml import loads as parse_toml
from django.conf import settings as django_settings
from typing import Any, Optional
from jsonschema import Draft7Validator
import re


def load_db_settings() -> dict:
    with open(django_settings.DATABASE_CONFIG_FILE, 'r') as file:
        data = parse_toml(file.read())
        file.close()

    return data


def to_camel_case(txt: str) -> str:
    fragments = txt.split('_')
    return fragments[0] + ''.join(x.title() for x in fragments[1:])


def to_snake_case(txt: str) -> str:
    return re.sub(r'(?<!^)(?=[A-Z])', '_', txt).lower()


def validate_schema(schema: dict, instance: Any) -> Optional[dict]:
    n_schema = Draft7Validator(schema)
    errors = [err.message for err in n_schema.iter_errors(instance)]
    if len(errors) == 0:
        return None
    return {
        'error': 'Schema validation failed',
        'description': errors,
    }