from psycopg2 import connect
from psycopg2.errors import lookup as lookup_db_error
from psycopg2.extensions import connection
from psycopg2.extras import RealDictCursor

from utils import load_db_settings

DB = None
_tries = 1
_max_tries = 3


def get_connection() -> connection:
	global DB, _tries, _max_tries
	if DB is None:
		try:
			DB = connect(**load_db_settings(), cursor_factory=RealDictCursor)
		except (lookup_db_error('08000'), lookup_db_error('08006')) as e:
			if _max_tries <= _tries:
				_raise_and_clean(e)
			else:
				_tries += 1
				return get_connection()

	_tries = 1
	return DB


def close():
	if DB is not None:
		DB.close()
		DB = None
	else:
		raise RuntimeError('The database connection is not open')


def _raise_and_clean(e):
	DB = None
	_tries = 1
	raise e