from utils import to_camel_case
from typing import Iterable, Tuple, Any


def camel_dict(iterable: Iterable[Tuple[str, Any]]) -> dict:
	res = dict()
	for k, v in dict(iterable).items():
		res[to_camel_case(k)] = v

	return res