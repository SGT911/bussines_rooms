from django.http import HttpResponse
from typing import Union, List, Callable
from functools import wraps
from json import dumps as json_dump, loads as json_parse


def method_not_allowed() -> HttpResponse:
    return HttpResponse('Method Not Allowed', content_type='text/plain', status=405)


def allow_methods(methods: Union[str, List[str]]) -> Callable:
    if isinstance(methods, str):
        methods = [methods]

    def decorator(fx: Callable):
        @wraps(fx)
        def wrapper(request):
            if request.method not in methods:
                return method_not_allowed()

            return fx(request)

        return wrapper

    return decorator

def get_body(fx):
    @wraps(fx)
    def wrapper(request):
        data = None
        if 'Content-Type' in request.headers and request.method != 'GET':
            if not request.headers['Content-Type'].strip().startswith('application/json'):
                return HttpResponse(json_dump({
                    'error': f"THis API only supports the content type 'application/json'"
                }), content_type='application/json', status=400)
            else:
                try:
                    data = json_parse(request.body)
                except Exception:
                    return HttpResponse(json_dump({
                        'error': 'The data is not parseable, check and retry',
                    }), content_type='application/json', status=500)

        return fx(request, data)

    return wrapper