"""bussines_rooms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from apps.core import views

urlpatterns = [
    path(r'', views.home, name='home'),
    path(r'clients/', views.clients, name='clients'),
    path(r'clients/add/', views.create_client, name='create_client'),
    path(r'clients/mod/<int:_id>/', views.modify_client, name='modify_client'),
    path(r'rooms/', views.rooms, name='rooms'),
    path(r'rooms/add/', views.create_room, name='create_room'),
    path(r'rooms/addHQ/', views.create_hq, name='create_headquarter'),
    path(r'events/', views.events, name='events'),
    path(r'events/add/', views.create_event, name='create_event'),
]
