from django.shortcuts import render, redirect
from django.urls import reverse
from utils.connection import get_connection
from functools import wraps
from apps.api.utils import availables_ages


def ensure_login(fx):
	@wraps(fx)
	def wrapper(request, **kwargs):
		if 'user_id' not in request.session:
			return redirect(reverse('users:login'))
		return fx(request, **kwargs)

	return wrapper


@ensure_login
def home(request):
	cur = get_connection().cursor()
	cur.execute('''
		SELECT
			identification,
			INITCAP(full_name) AS full_name,
			is_admin AS admin
		FROM users
			WHERE id = %s
	''', (request.session['user_id'],))

	user_data = cur.fetchone()
	cur.close()

	return render(request, 'index.html', dict(user_data=user_data))


@ensure_login
def clients(request):
	return render(request, 'clients.html')

@ensure_login
def create_client(request):
	return render(request, 'create_client.html', dict(ages=availables_ages()))

@ensure_login
def modify_client(request, _id: int):
	cur = get_connection().cursor()
	cur.execute('''
		SELECT
			id,
			INITCAP(first_name) AS first_name,
			INITCAP(second_name) AS second_name,
			INITCAP(first_last_name) AS first_last_name,
			INITCAP(second_last_name) AS second_last_name,
			identification,
			identification_vd,
			country,
			state,
			city,
			age
		FROM clients
			WHERE id = %s
			LIMIT 1
	''', (_id,))

	client_data = dict(cur.fetchone())
	if client_data is None:
		cur.close()
		return render(request, 'errors/notfound.html')

	for k, v in client_data.items():
		if v is None:
			client_data[k] = ''

	cur.execute('''
		SELECT
			reg_code,
			number
		FROM cli_phones
			WHERE cli_id = %s
	''', (_id,))

	phones = [dict(el) for el in cur.fetchall()]

	cur.execute('''
		SELECT
			username || '@' || domain AS email
		FROM cli_mails
			WHERE cli_id = %s
	''', (_id,))

	emails = [el['email'] for el in cur.fetchall()]

	cur.close()

	return render(request, 'modify_client.html', dict(client=client_data, phones=phones, emails=emails, ages=availables_ages()))

@ensure_login
def rooms(request):
	return render(request, 'rooms.html')

@ensure_login
def create_hq(request):
	return render(request, 'create_headquarter.html')

@ensure_login
def create_room(request):
	cur = get_connection().cursor()
	cur.execute('''
		SELECT
			id,
			name
		FROM headquarters
	''')

	headquarters = cur.fetchall()

	cur.close()

	return render(request, 'create_room.html', dict(headquarters=headquarters))

@ensure_login
def events(request):
	return render(request, 'events.html')

@ensure_login
def create_event(request):
	cur = get_connection().cursor()

	cur.execute('''
		SELECT
			id,
			identification,
			full_name
		FROM v_clients
	''')
	clients = cur.fetchall()

	cur.execute('''
		SELECT
			id,
			hq_id,
			hq_name,
			name,
			max_capacity
		FROM v_rooms
	''')
	rooms = cur.fetchall()

	cur.execute('''
		SELECT
			id,
			INITCAP(name) AS name
		FROM event_types
	''')
	event_types = cur.fetchall()

	cur.close()

	return render(request, 'create_event.html', dict(
		user=request.session['user_id'],
		clients=clients,
		rooms=rooms,
		event_types=event_types,
	))