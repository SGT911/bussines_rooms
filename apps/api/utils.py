from utils.connection import get_connection
from typing import List


def availables_ages() -> List[str]:
	cur = get_connection().cursor()

	cur.execute('''
		SELECT
			choices
		FROM age_range_choices
	''')
	data = [el['choices'] for el in cur.fetchall()]

	cur.close()
	return data