from django.http import HttpResponse

from json import dumps as json_dump

from utils import to_snake_case, validate_schema
from utils.connection import get_connection, lookup_db_error
from utils.http import allow_methods, get_body
from utils.dict import camel_dict
from apps.api import schemas
from datetime import datetime, timezone, timedelta


@allow_methods(['GET', 'POST', 'PUT'])
@get_body
def clients(request, data):
    cur = get_connection().cursor()
    if request.method == 'GET':
        cur.execute('''
            SELECT
                *
            FROM v_clients
        ''')

        data = [camel_dict(el) for el in cur.fetchall()]
        for i in range(len(data)):
            data[i]['createdAt'] = data[i]['createdAt'].isoformat()

        return HttpResponse(json_dump(data), content_type='application/json', status=200)
    elif request.method == 'POST':
        err = validate_schema(schemas.CLIENTS_POST, data)
        if err is not None:
            return HttpResponse(json_dump(err), content_type='application/json', status=400)

        for v in ['secondName', 'secondLastName', 'identificationVd']:
            if v not in data:
                data[v] = None

        try:
            cur.execute('''
                CALL create_client(
                    %(firstName)s::VARCHAR(30),
                    %(secondName)s::VARCHAR(30),
                    %(firstLastName)s::VARCHAR(30),
                    %(secondLastName)s::VARCHAR(30),
                    %(identification)s::BIGINT,
                    %(identificationVd)s::SMALLINT,
                    %(country)s::CHAR(2),
                    %(state)s::VARCHAR(35),
                    %(city)s::VARCHAR(30),
                    %(age)s::age_range,
                    %(emails)s::VARCHAR[],
                    %(phones)s::VARCHAR[]
                )
            ''', data)
        except lookup_db_error('23505'):
            get_connection().rollback()
            cur.close()
            return HttpResponse(json_dump({
                'error': 'The client already exist',
            }), content_type='application/json', status=400)
        else:
            get_connection().commit()
            cur.close()

            return HttpResponse(json_dump({
                'payload': 'ok',
            }), content_type='application/json', status=201)
    else:
        err = validate_schema(schemas.CLIENTS_PUT, data)
        if err is not None:
            return HttpResponse(json_dump(err), content_type='application/json', status=400)

        available_fields = [
            'firstName',
            'secondName',
            'firstLastName',
            'secondLastName',
            'country',
            'state',
            'city',
            'age',
        ]

        cli_id = data['id']
        del data['id']

        for k, v in data.items():
            if k not in available_fields:
                return HttpResponse(json_dump({
                    'error': f"Unknown field '{k}' check and try again",
                }), content_type='application/json', status=400)

            try:
                cur.execute(f'''
                    UPDATE clients SET {to_snake_case(k)} = %s WHERE id = %s
                ''', (v, cli_id))
            except Exception:
                get_connection().rollback()
                cur.close()
                return HttpResponse(json_dump({
                    'error': 'Unexpected error, please contact the web master',
                }), content_type='application/json', status=500)

        get_connection().commit()
        cur.close()

        return HttpResponse(json_dump({
            'payload': 'ok',
        }), content_type='application/json', status=200)


@allow_methods(['POST', 'DELETE'])
@get_body
def client_emails(request, data):
    err = validate_schema(schemas.CLIENT_EMAILS, data)
    if err is not None:
        return HttpResponse(json_dump(err), content_type='application/json', status=400)

    cur = get_connection().cursor()
    if request.method == 'POST':
        try:
            cur.execute('''
                CALL add_client_email(%(id)s, %(email)s)
            ''', data)
        except lookup_db_error('23505'):
            get_connection().rollback()
            cur.close()
            return HttpResponse(json_dump({
                'error': 'The email already exist, try with other',
            }), content_type='application/json', status=400)
        else:
            get_connection().commit()
            return HttpResponse(json_dump({
                'payload': 'ok',
            }), content_type='application/json', status=201)
    else:
        cur.execute('''
            DELETE FROM cli_mails
                WHERE usename || '@' || domain = %(email)s AND cli_id = %(id)s
        ''', data)

        if cur.rowcount == 0:
            get_connection().rollback()
            cur.close()
            return HttpResponse(json_dump({
                'error': 'The email does not exist',
            }), content_type='application/json', status=404)
        else:
            get_connection().commit()
            return HttpResponse(json_dump({
                'payload': 'ok',
            }), content_type='application/json', status=200)


@allow_methods(['POST', 'DELETE'])
@get_body
def client_phones(request, data):
    err = validate_schema(schemas.CLIENT_PHONES, data)
    if err is not None:
        return HttpResponse(json_dump(err), content_type='application/json', status=400)

    cur = get_connection().cursor()
    if request.method == 'POST':
        try:
            cur.execute('''
                CALL add_client_phone(%(id)s, %(phone)s)
            ''', data)
        except lookup_db_error('23505'):
            get_connection().rollback()
            cur.close()
            return HttpResponse(json_dump({
                'error': 'The phone already exist, try with other',
            }), content_type='application/json', status=400)
        else:
            get_connection().commit()
            return HttpResponse(json_dump({
                'payload': 'ok',
            }), content_type='application/json', status=201)
    else:
        cur.execute('''
            DELETE FROM cli_phones
                WHERE reg_code || ' ' || number = %(phone)s AND cli_id = %(id)s
        ''', data)

        if cur.rowcount == 0:
            get_connection().rollback()
            cur.close()
            return HttpResponse(json_dump({
                'error': 'The phone does not exist',
            }), content_type='application/json', status=404)
        else:
            get_connection().commit()
            return HttpResponse(json_dump({
                'payload': 'ok',
            }), content_type='application/json', status=200)


@allow_methods(['GET', 'POST'])
@get_body
def headquarters(request, data):
    cur = get_connection().cursor()
    if request.method == 'GET':
        cur.execute('''
            SELECT
                *
            FROM headquarters
        ''')

        data = [camel_dict(el) for el in cur.fetchall()]
        cur.close()
        return HttpResponse(json_dump(data), content_type='application/json', status=200)
    else:
        err = validate_schema(schemas.HEADQUARTERS, data)
        if err is not None:
            cur.close()
            return HttpResponse(json_dump(err), content_type='application/json', status=400)

        try:
            cur.execute('''
                CALL add_headquarter(
                    %(name)s,
                    %(country)s,
                    %(state)s,
                    %(city)s,
                    %(locationDetail)s
                )
            ''', data)
        except lookup_db_error('23505'):
            get_connection().rollback()
            cur.close()
            return HttpResponse(json_dump({
                'error': 'The headquarter already exist',
            }), content_type='application/json', status=400)
        else:
            get_connection().commit()
            cur.close()

            return HttpResponse(json_dump({
                'payload': 'ok',
            }), content_type='application/json', status=201)


@allow_methods(['GET', 'POST'])
@get_body
def rooms(request, data):
    cur = get_connection().cursor()
    if request.method == 'GET':
        cur.execute('''
            SELECT
                *
            FROM v_rooms
        ''')

        data = [camel_dict(el) for el in cur.fetchall()]
        return HttpResponse(json_dump(data), content_type='application/json', status=200)
    else:
        err = validate_schema(schemas.ROOMS, data)
        if err is not None:
            cur.close()
            return HttpResponse(json_dump(err), content_type='application/json', status=400)

        try:
            cur.execute('''
                CALL add_room(
                    %(headquarter)s,
                    %(floor)s::SMALLINT,
                    %(name)s::VARCHAR(35),
                    %(maxCapacity)s
                )
            ''', data)
        except lookup_db_error('23505'):
            get_connection().rollback()
            cur.close()
            return HttpResponse(json_dump({
                'error': 'The room already exist',
            }), content_type='application/json', status=400)
        else:
            get_connection().commit()
            cur.close()

            return HttpResponse(json_dump({
                'payload': 'ok',
            }), content_type='application/json', status=201)


@allow_methods(['POST', 'DELETE'])
@get_body
def events(request, data):
    cur = get_connection().cursor()
    if request.method == 'POST':
        err = validate_schema(schemas.EVENTS_POST, data)
        if err is not None:
            cur.close()
            return HttpResponse(json_dump(err), content_type='application/json', status=400)

        cur.execute('''
            SELECT
                max_capacity
            FROM rooms
                WHERE id = %(room)s AND hq_id = %(headquarter)s
            LIMIT 1
        ''', data)
        if cur.rowcount == 0:
            cur.close()
            return HttpResponse(json_dump({
                'error': 'Room does not exist',
            }), content_type='application/json', status=404)
        elif cur.fetchone()['max_capacity'] < data['peopleCount']:
            cur.close()
            return HttpResponse(json_dump({
                'error': 'The room is too small for the people',
            }), content_type='application/json', status=400)
        
        cur.execute('''
            SELECT
                COUNT(1) AS count
            FROM events
                WHERE room_id = %(room)s AND hq_id = %(headquarter)s AND event_at = %(eventDate)s
        ''', data)
        if cur.fetchone()['count'] > 0:
            cur.close()
            return HttpResponse(json_dump({
                'error': 'The room is busy in that date',
            }), content_type='application/json', status=400)

        cur.execute('''
            CALL create_event(
                %(user)s,
                %(client)s,
                %(headquarter)s,
                %(room)s,
                %(eventType)s,
                %(peopleCount)s,
                %(eventDate)s
            )
        ''', data)

        get_connection().commit()
        cur.close()

        return HttpResponse(json_dump({
            'payload': 'ok',
        }), content_type='application/json', status=201)
    else:
        err = validate_schema(schemas.EVENTS_DELETE, data)
        if err is not None:
            cur.close()
            return HttpResponse(json_dump(err), content_type='application/json', status=400)

        cur.execute('''
            DELETE FROM events
                WHERE id = %(id)s
        ''', data)

        if cur.rowcount == 0:
            get_connection().rollback()
            cur.close()
            return HttpResponse(json_dump({
                'error': 'The event does not exist',
            }), content_type='application/json', status=404)
        else:
            get_connection().commit()
            return HttpResponse(json_dump({
                'payload': 'ok',
            }), content_type='application/json', status=200)

@allow_methods(['PUT'])
@get_body
def confirm_event(request, data):
    err = validate_schema(schemas.CONFIRM_EVENT, data)
    if err is not None:
        return HttpResponse(json_dump(err), content_type='application/json', status=400)

    cur = get_connection().cursor()
    cur.execute('''
        SELECT
            COUNT(1) AS count
        FROM events WHERE id = %(id)s
            LIMIT 1
    ''', data)
    if cur.rowcount == 0:
            get_connection().rollback()
            cur.close()
            return HttpResponse(json_dump({
                'error': 'The event does not exist',
            }), content_type='application/json', status=404)

    cur.execute('''
        CALL alter_confirm_event(%(id)s, %(state)s)
    ''', data)

    get_connection().commit()
    return HttpResponse(json_dump({
        'payload': 'ok',
    }), content_type='application/json', status=200)


@allow_methods(['GET'])
def events_summary(request):
    if 'confirmed' in request.GET and request.GET['confirmed'] == 'yes':
        sql = '''
            SELECT
                id,
                name,
                identification,
                phones,
                country,
                state,
                city,
                location_detail,
                event_at,
                people_count,
                INITCAP(event_type) AS event_type,
                is_confirmed
            FROM events_summary
                WHERE is_confirmed = TRUE AND
                    event_at >= %(from)s AND event_at <= %(to)s
        '''
    else:
        sql = '''
            SELECT
                id,
                name,
                identification,
                phones,
                country,
                state,
                city,
                location_detail,
                event_at,
                people_count,
                INITCAP(event_type) AS event_type,
                is_confirmed
            FROM events_summary
                WHERE event_at >= %(from)s AND event_at <= %(to)s
        '''

    data = {
        'from': datetime.min,
        'to': datetime.max
    }
    if 'from' in request.GET:
        if 'to' not in request.GET:
            return HttpResponse(json_dump({
                'error': "The field 'to' is required",
            }), content_type='application/json', status=400)

        try:
            data['from'] = datetime.fromisoformat(request.GET['from'].replace('Z',''))
            
            if request.GET['to'] != 'max':
                data['to'] = datetime.fromisoformat(request.GET['to'].replace('Z',''))
        except Exception as e:
            print(e)
            return HttpResponse(json_dump({
                'error': 'Can not parse requests params, check and try again',
            }), content_type='application/json', status=400)
        else:
            if data['from'].tzinfo == None:
                data['from'] = data['from'].replace(tzinfo=timezone(timedelta(seconds=0)))
            if data['to'].tzinfo == None and data['to'] != datetime.max:
                data['to'] = data['to'].replace(tzinfo=timezone(timedelta(seconds=0)))


    cur = get_connection().cursor()
    cur.execute(sql, data)

    data = [camel_dict(el) for el in cur.fetchall()]
    for i in range(len(data)):
        data[i]['eventAt'] = data[i]['eventAt'].isoformat()

    return HttpResponse(json_dump(data), content_type='application/json', status=200)