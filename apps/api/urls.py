"""bussines_rooms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from apps.api import views

urlpatterns = [
    path(r'clients/', views.clients, name='clients'),
    path(r'clients/emails/', views.client_emails, name='client_emails'),
    path(r'clients/phone/', views.client_phones, name='client_phones'),
    path(r'headquarters/', views.headquarters, name='headquarters'),
    path(r'rooms/', views.rooms, name='rooms'),
    path(r'events/', views.events, name='events'),
    path(r'events/confirm/', views.confirm_event, name='confirm_event'),
    path(r'events/summary/', views.events_summary, name='events_summary'),
]
