from apps.api.utils import availables_ages

CLIENTS_POST = {
    'type': 'object',
    'required': [
        'firstName',
        'firstLastName',
        'identification',
        'country',
        'state',
        'city',
        'age',
        'emails',
        'phones',
    ],
    'additionalProperties': False,
    'properties': {
        'firstName': {
            'type': 'string',
        },
        'secondName': {
            'type': 'string',
        },
        'firstLastName': {
            'type': 'string',
        },
        'secondLastName': {
            'type': 'string',
        },
        'identification': {
            'type': 'integer',
            'minimum': 100000,
        },
        'identificationVd': {
            'type': 'integer',
            'minimum': 1,
            'maximum': 9,
        },
        'country': {
            'type': 'string',
            'pattern': r'[A-Z]{2}',
        },
        'state': {
            'type': 'string',
        },
        'city': {
            'type': 'string',
        },
        'age': {
            'type': 'string',
            'enum': availables_ages(),
        },
        'emails': {
            'type': 'array',
            'uniqueItems': True,
            'items': {
                'type': 'string',
            },
        },
        'phones': {
            'type': 'array',
            'uniqueItems': True,
            'items': {
                'type': 'string',
            },
        },
    },
}

CLIENTS_PUT = {
    'type': 'object',
    'required': [
        'id',
    ],
    'additionalProperties': False,
    'properties': {
        'id': {
            'type': 'integer',
            'minimum': 1,
        },
        'firstName': {
            'type': 'string',
        },
        'secondName': {
            'type': 'string',
        },
        'firstLastName': {
            'type': 'string',
        },
        'secondLastName': {
            'type': 'string',
        },
        'country': {
            'type': 'string',
            'pattern': r'[A-Z]{2}',
        },
        'state': {
            'type': 'string',
        },
        'city': {
            'type': 'string',
        },
        'age': {
            'type': 'string',
            'enum': availables_ages(),
        },
    },
}

CLIENT_EMAILS = {
    'type': 'object',
    'required': ['id', 'email'],
    'additionalProperties': False,
    'properties': {
        'id': {
            'type': 'integer',
            'minimum': 1,
        },
        'email': {
            'type': 'string',
        },
    },
}

CLIENT_PHONES = {
    'type': 'object',
    'required': ['id', 'phone'],
    'additionalProperties': False,
    'properties': {
        'id': {
            'type': 'integer',
            'minimum': 1,
        },
        'phone': {
            'type': 'string',
        },
    },
}

HEADQUARTERS = {
    'type': 'object',
    'required': [
        'name',
        'country',
        'state',
        'city',
        'locationDetail',
    ],
    'additionalProperties': False,
    'properties': {
        'name': {
            'type': 'string',
        },
        'country': {
            'type': 'string',
            'pattern': r'[A-Z]{2}',
        },
        'state': {
            'type': 'string',
        },
        'city': {
            'type': 'string',
        },
        'locationDetail': {
            'type': 'string',
        },
    },
}

ROOMS = {
    'type': 'object',
    'required': [
        'headquarter',
        'floor',
        'name',
        'maxCapacity',
    ],
    'additionalProperties': False,
    'properties': {
        'headquarter': {
            'type': 'integer',
            'minimum': 1
        },
        'floor': {
            'type': 'integer',
            'minimum': 1
        },
        'name': {
            'type': 'string',
        },
        'maxCapacity': {
            'type': 'integer',
        },
    },
}

EVENTS_POST = {
    'type': 'object',
    'required': [
        'user',
        'client',
        'headquarter',
        'room',
        'eventType',
        'peopleCount',
        'eventDate',
    ],
    'additionalProperties': False,
    'properties': {
        'user': {
            'type': 'integer',
            'minimum': 1,
        },
        'client': {
            'type': 'integer',
            'minimum': 1,
        },
        'headquarter': {
            'type': 'integer',
            'minimum': 1,
        },
        'room': {
            'type': 'integer',
            'minimum': 1,
        },
        'eventType': {
            'type': 'integer',
            'minimum': 1,
        },
        'peopleCount': {
            'type': 'integer',
        },
        'eventDate': {
            'type': 'string',
            'format': 'date-time',
        },
    },
}

EVENTS_DELETE = {
    'type': 'object',
    'required': ['id'],
    'additionalProperties': False,
    'properties': {
        'id': {
            'type': 'integer',
            'minimum': 1,
        },
    },
}

CONFIRM_EVENT = {
    'type': 'object',
    'required': ['id', 'state'],
    'additionalProperties': False,
    'properties': {
        'id': {
            'type': 'integer',
            'minimum': 1,
        },
        'state': {
            'type': 'boolean',
        },
    },
}