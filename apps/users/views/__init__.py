from django.shortcuts import render, redirect
from django.urls import reverse

from utils.connection import get_connection
from utils.http import allow_methods


@allow_methods('GET')
def login_page(request):
    if 'user_id' in request.session:
        return redirect(reverse('core:home'))

    cur = get_connection().cursor()
    cur.execute('''
        SELECT
            COUNT(1) AS count
        FROM auth_users
    ''')

    if cur.fetchone()['count'] == 0:
        return redirect(reverse('users:install'))

    return render(request, 'login.html')


@allow_methods('GET')
def install_users(request):
    cur = get_connection().cursor()
    cur.execute('''
        SELECT
            COUNT(1) AS count
        FROM auth_users
    ''')

    if cur.fetchone()['count'] > 0:
        return redirect(reverse('users:login'))

    return render(request, 'install.html')


@allow_methods('GET')
def admin_users(request):
    if 'user_id' not in request.session:
        if 'admin' in request.session and not request.session['admin']:
            return redirect(reverse('core:home'))
        else:
            return redirect(reverse('users:login'))

    cur = get_connection().cursor()
    cur.execute('''
        SELECT
            id,
            identification,
            INITCAP(full_name) AS full_name,
            is_admin,
            CASE
                WHEN %(id)s = id
                    THEN TRUE
                ELSE FALSE
            END AS its_me
        FROM users
        ORDER BY created_at ASC
    ''', dict(id=request.session['user_id']))

    users = cur.fetchall()
    return render(request, 'admin.html', dict(users=users, len_users=len(users)))


@allow_methods('GET')
def create_user(request):
    if 'user_id' not in request.session:
        if 'admin' in request.session and not request.session['admin']:
            return redirect(reverse('core:home'))
        else:
            return redirect(reverse('users:login'))

    return render(request, 'create_user.html')


@allow_methods('GET')
def change_password(request):
    if 'user_id' not in request.session:
        return redirect(reverse('users:login'))

    return render(request, 'change_password.html')


@allow_methods('GET')
def logout(request):
    del request.session['user_id']
    del request.session['admin']

    return redirect(reverse('core:home'))