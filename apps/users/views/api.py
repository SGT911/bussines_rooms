from django.http import HttpResponse

from json import dumps as json_dump

from utils import validate_schema
from utils.connection import get_connection, lookup_db_error
from utils.http import allow_methods, get_body
from apps.users import schemas


@allow_methods('POST')
@get_body
def login(request, data):
    err = validate_schema(schemas.LOGIN, data)
    if err is not None:
        return HttpResponse(json_dump(err), content_type='application/json', status=400)

    cur = get_connection().cursor()
    cur.execute('''
        SELECT
            id,
            is_admin
        FROM auth_users
            WHERE identification = %(identification)s AND passwd = HASH_PASSWD(%(password)s)
            LIMIT 1
    ''', data)

    if cur.rowcount == 0:
        return HttpResponse(json_dump({
            'error': f"The credential of the user '{data['identification']}' not exists or are incorrects",
        }), content_type='application/json', status=404)

    res = cur.fetchone()
    request.session['user_id'] = res['id']
    request.session['admin'] = res['is_admin']
    return HttpResponse(json_dump({
        'payload': 'ok',
    }), content_type='application/json', status=200)


@allow_methods('POST')
@get_body
def install_users(request, data):
    err = validate_schema(schemas.INSTALL_USERS, data)
    if err is not None:
        return HttpResponse(json_dump(err), content_type='application/json', status=400)

    cur = get_connection().cursor()
    try:
        cur.execute('''
            CALL create_admin(%(identification)s, %(fullName)s, %(password)s)
        ''', data)
    except lookup_db_error('23505'):
        get_connection().rollback()
        cur.close()
        return HttpResponse(json_dump({
            'error': f"The User '{data['identification']}' already exist",
        }), content_type='application/json', status=400)

    get_connection().commit()
    cur.close()
    return HttpResponse(json_dump({
        'payload': 'ok',
    }), content_type='application/json', status=201)


@allow_methods('PUT')
@get_body
def alter_user_admin(request, data):
    err = validate_schema(schemas.ALTER_USER_ADMIN, data)
    if err is not None:
        return HttpResponse(json_dump(err), content_type='application/json', status=400)

    cur = get_connection().cursor()
    cur.execute('''
        CALL alter_user_admin(%(id)s, %(admin)s)
    ''', data)

    get_connection().commit()
    cur.close()
    return HttpResponse(json_dump({
        'payload': 'ok',
    }), content_type='application/json', status=200)


@allow_methods('POST')
@get_body
def create_user(request, data):
    err = validate_schema(schemas.CREATE_USER, data)
    if err is not None:
        return HttpResponse(json_dump(err), content_type='application/json', status=400)

    cur = get_connection().cursor()
    try:
        cur.execute('''
            CALL create_user(%(identification)s, %(fullName)s, %(password)s)
        ''', data)
    except lookup_db_error('23505'):
        get_connection().rollback()
        cur.close()
        return HttpResponse(json_dump({
            'error': f"The User '{data['identification']}' already exist",
        }), content_type='application/json', status=400)
    else:
        if data['admin']:
            cur.execute('''
                SELECT id FROM users WHERE identification = %(identification)s LIMIT 1
            ''', data)

            data['id'] = cur.fetchone()['id']

            cur.execute('''
                CALL alter_user_admin(%(id)s, %(admin)s)
            ''', data)

    get_connection().commit()
    cur.close()
    return HttpResponse(json_dump({
        'payload': 'ok',
    }), content_type='application/json', status=201)


@allow_methods('PUT')
@get_body
def change_password(request, data):
    err = validate_schema(schemas.CHANGE_PASSWORD, data)
    if err is not None:
        return HttpResponse(json_dump(err), content_type='application/json', status=400)

    data['id'] = request.session['user_id']
    cur = get_connection().cursor()

    cur.execute('''
        SELECT
            COUNT(1) AS count
        FROM auth_users
            WHERE id = %(id)s AND passwd =  HASH_PASSWD(%(password)s)
            LIMIT 1
    ''', data)

    if cur.fetchone()['count'] == 0:
        return HttpResponse(json_dump({
            'error': 'The credentials are incorrects',
        }), content_type='application/json', status=401)    

    cur.execute('''
        CALL user_change_password(%(id)s, %(newPassword)s)
    ''', data)

    get_connection().commit()
    cur.close()
    return HttpResponse(json_dump({
        'payload': 'ok',
    }), content_type='application/json', status=200)