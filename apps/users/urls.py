"""bussines_rooms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from apps.users import views
from apps.users.views import api

urlpatterns = [
	path(r'login/', views.login_page, name='login'),
	path(r'logout/', views.logout, name='logout'),
	path(r'install/', views.install_users, name='install'),
	path(r'admin/', views.admin_users, name='admin'),
	path(r'admin/add/', views.create_user, name='create'),
	path(r'changePassword/', views.change_password, name='change_password'),

	# Api views
	path(r'api/login/', api.login, name='api_login'),
	path(r'api/install/', api.install_users, name='api_install'),
	path(r'api/admin/', api.alter_user_admin, name='api_alter_user_admin'),
	path(r'api/create/', api.create_user, name='api_create'),
	path(r'api/password/', api.change_password, name='api_change_password'),
]
