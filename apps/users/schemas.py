LOGIN = {
    'type': 'object',
    'required': [
        'identification',
        'password',
    ],
    'additionalProperties': False,
    'properties': {
        'identification': {
            'type': 'integer',
            'minimum': 100000,
        },
        'password': {
            'type': 'string',
        },
    },
}

INSTALL_USERS = {
    'type': 'object',
    'required': [
        'identification',
        'fullName',
        'password',
    ],
    'additionalProperties': False,
    'properties': {
        'identification': {
            'type': 'integer',
            'minimum': 100000,
        },
        'fullName': {
            'type': 'string',
        },
        'password': {
            'type': 'string',
        },
    },
}

ALTER_USER_ADMIN = {
    'type': 'object',
    'required': [
        'id',
        'admin',
    ],
    'additionalProperties': False,
    'properties': {
        'id': {
            'type': 'integer',
            'minimum': 1,
        },
        'admin': {
            'type': 'boolean',
        },
    },
}

CREATE_USER = {
    'type': 'object',
    'required': [
        'identification',
        'fullName',
        'password',
        'admin',
    ],
    'additionalProperties': False,
    'properties': {
        'identification': {
            'type': 'integer',
            'minimum': 100000,
        },
        'fullName': {
            'type': 'string',
        },
        'password': {
            'type': 'string',
        },
        'admin': {
            'type': 'boolean',
        },
    },
}

CHANGE_PASSWORD = {
    'type': 'object',
    'required': [
        'password',
        'newPassword',
    ],
    'additionalProperties': False,
    'properties': {
        'password': {
            'type': 'string',
        },
        'newPassword': {
            'type': 'string',
        },
    },
}