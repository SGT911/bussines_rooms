# Bussines Rooms
**Author:** [SGT911](https://gitlab.com/SGT911)
**Lisence:** *None*

## Technologies
- Backend: [**Python**](https://www.python.org/) with framework [*Django*](https://www.djangoproject.com/)
- Frontend: [**JavaScript**](https://developer.mozilla.org/es/docs/Web/JavaScript) vanilla
- Database: [**PostgreSQL**](https://www.postgresql.org/)
- CSS Framework: [**Bootstrap**](https://getbootstrap.com/)